<%@ include file="../../header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form method="post">

	<h1>Conversion</h1>

	<p>
		<label>Quantit�<input type="number" step="0.01"
			name="quantite" min="0"></label>
	</p>
	<p>
		<label>unit�<select name="unite">
				<c:forEach items="${ unites }" var="u">
					<option>${u}</option>
				</c:forEach>

		</select></label>
	<p>
		<input type="submit" value="Convertir">${message}</p>

	<table>
		<thead>
			<tr>
				<c:forEach items="${ unites }" var="c">
					<th>${c}</th>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach items="${ convertedValues }" var="c">
					<td>${c}</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>

</form>