<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css_files/page.css">
<title>Temps de Décongélation</title>
</head>


<body>

	<form method="post">
		<h1>Temps de décongélation</h1>

		<label for="masse" id="masse"> Votre masse en kg </label> <input
			type="number" name="masse"> <br>
		<p id="ErrMsg">
			<input type="submit"> ${message}
		</p>


	</form>

	<%@ include file="../../footer.jsp"%>
</body>
</html>