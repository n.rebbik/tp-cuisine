function time(date) {

	if ((date.getHours() >=0) && (date.getMinutes() >=0) && (date.getSeconds() >=0)) {

		date.setSeconds(date.getSeconds()-1);
		document.getElementById("hour").value = date.getHours() 
		document.getElementById("minute").value = date.getMinutes()
		second = document.getElementById("second").value = date.getSeconds()

		t = setTimeout (function() {time(date)}, 1000);

	}

}

function launch() {

	hour = document.getElementById("hour").value;
	minute = document.getElementById("minute").value;
	second = document.getElementById("second").value;
	date = new Date(0,0,0, hour, minute, second);

	time(date);
}

function resetElementToNull() {
	document.getElementById("hour").value = 0;
	document.getElementById("minute").value = 0;
	document.getElementById("second").value = 0;
}


function reset() {

	if(typeof t === 'undefined'){
		resetElementToNull()
	}
	else{
		clearTimeout(t)
		setTimeout(function() {
			resetElementToNull()
		}, 500)
		t = undefined
	}




}

