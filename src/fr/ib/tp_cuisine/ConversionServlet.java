package fr.ib.tp_cuisine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class ConversionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private List<String> unites = Arrays.asList("g", "kg", "l", "cuillère à café", "cuillère à soupe", "pincée", "tasse");

	public double converter (double x, double a) {
		return Math.round(a*x * 1000.0) / 1000.0;
	}

	public List<List<Double>> converterValueFromCsv() {
		List<List<Double>> superList = new ArrayList<List<Double>>();

		try {
			String path = getServletContext().getRealPath("/data_files/conversion.txt");
			FileReader file = new FileReader(path);
			BufferedReader csvReader = new BufferedReader(file);
			String row = csvReader.readLine();

			while (row != null) {
				String[] data = row.split(";");
				List<Double> dataToDouble = new ArrayList<>();
				for (String s:data) {
					double d = Double.parseDouble(s);
					dataToDouble.add(d);
				}

				superList.add(dataToDouble);
				row = csvReader.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return superList;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setAttribute("unites", unites);
		req.getRequestDispatcher("/WEB-INF/vues/conversion.jsp").forward(req,  resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String quantiteString = req.getParameter("quantite");
		
		if (quantiteString != null && !quantiteString.trim().equals("")) {

			double quantite = Integer.parseInt(quantiteString);
			String unite = req.getParameter("unite");

			if (quantite <= 0) {
				req.setAttribute("message", "valeur incorrect");
			} else {
				List<List<Double>> coefConvertor = converterValueFromCsv();
				List<Double> uniteValues = coefConvertor.get(this.unites.indexOf(unite));
				
				List<Double> finalConvertedValues = new ArrayList<Double>();

				for (double value:uniteValues) {
					finalConvertedValues.add(converter(quantite, value));
				}
				
				req.setAttribute("convertedValues", finalConvertedValues);
				
			}
		
		}
		doGet(req, resp);

	}

}
