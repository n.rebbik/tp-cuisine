package fr.ib.tp_cuisine;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DecongelationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/WEB-INF/vues/decongelation.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		int masse; 

		if(req.getParameter("masse")!="") 
		{

			masse = Integer.parseInt(req.getParameter("masse"));
			req.setAttribute("air", masse*60*10);
			req.setAttribute("four", masse*12);
			req.getRequestDispatcher("WEB-INF/vues/tempscalcule.jsp").forward(req, resp);

		}
		else
		{
			req.setAttribute("message","Veuillez saisir votre masse en kg!");
			doGet(req,resp);

		}
	}






}

